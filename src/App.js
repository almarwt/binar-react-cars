import './App.css';
import { LandingPage } from './components';

function App() {
  return (
    <div className="App" style={{padding: '30px'}}>
      <LandingPage/>
    </div>
  );
}

export default App;
